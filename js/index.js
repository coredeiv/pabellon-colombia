'use strict'

function closeModal() {
  document.getElementById("closeModal").addEventListener("click", (e) => {
    e.preventDefault();
    document.getElementById("modals").classList.remove("active");
  });
}

function viewMore() {
  document.getElementById("viewMore").addEventListener("click", (e) => {
    e.preventDefault();
    document.getElementById("viewMore").classList.toggle("active");
    document.querySelector(".tp-content_col--left-mobile").classList.toggle("tp-dblock-mobile");
    document.querySelector(".tp-content_col--right").classList.toggle("tp-dblock-mobile");
  });
}

document.addEventListener ('DOMContentLoaded', function(event) {
  
  closeModal();
  viewMore();

});